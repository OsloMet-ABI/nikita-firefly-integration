package app;

import app.domain.BroadcastResult;
import app.domain.DataResult;
import app.integration.crud.CRUDMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.*;

import static app.Constants.NOARK_QUEUE;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
public class MessageReceiver {
    private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

    public MessageReceiver() {
    }

    @RabbitListener(queues = NOARK_QUEUE)
    public void processMessage(String incoming) {
        // Make note of time use from start to finish
        Instant start = Instant.now();
        Map<String, String> environment = System.getenv();
        String baseUrl = environment.get("RKBCIA_URI");
        String authToken = environment.get("RKBCIA_TOKEN");

        // If we don't have a baseUrl or authToken the throw message and return
        if (null == authToken || null == baseUrl) {
            logger.error("authToken or baseUrl is null. Set RKBCIA_URI/RKBCIA_TOKEN values");
            return;
        }

        if (authToken.isBlank() || baseUrl.isBlank()) {
            logger.error("authToken or baseUrl is null. Set RKBCIA_URI/RKBCIA_TOKEN values");
            return;
        }

        ObjectMapper objectMapper = new ObjectMapper();
        CRUDMessage crudMessage;
        try {
            crudMessage = objectMapper.readValue(incoming, CRUDMessage.class);
        } catch (IOException e) {
            logger.error("Cannot parse incoming message (" + e.getMessage() + ")");
            return;
        }

        String emailRegex = "bruker_(\\d+)@example.com";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(crudMessage.createdBy());
        if (matcher.matches()) {
            String number = matcher.group(1);
            try {
                int id_num = Integer.parseInt(number);
                if (id_num >= 30)
                    return;
            } catch (NumberFormatException ex) {
                logger.error(ex.getMessage());
            }
        }
        if (null == crudMessage) {
            logger.error("Incoming message is parsed to null");
            return;
        }


        logger.info("Incoming message is :" + crudMessage);
        RestClient restClient = RestClient.builder()
                .baseUrl(baseUrl)
                .defaultHeader(AUTHORIZATION, authToken)
                .build();

        // Step 1:
        // Create a payload and upload it
        JSONObject payload = getJsonObject(crudMessage);

        try {
            // Known value from documentation
            String uriData = "/api/v1/data";

            // Post data to the blockchain node
            DataResult dataResult = restClient
                    .post()
                    .uri(uriData)
                    .contentType(APPLICATION_JSON)
                    .body(payload.toString())
                    .retrieve()
                    .body(DataResult.class);

            // Step 2:
            // Broadcast message about data to blockchain
            String uuidData = dataResult.getId();
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("id", uuidData);
            JSONArray dataArray = new JSONArray();
            dataArray.put(dataMap);
            JSONObject broadcast = new JSONObject();
            broadcast.put("data", dataArray);

            String namespace = dataResult.getNamespace();
            String uriBroadcast = "/api/v1/namespaces/" + namespace + "/messages/broadcast";


            BroadcastResult broadcastResult = restClient
                    .post()
                    .uri(uriBroadcast)
                    .contentType(APPLICATION_JSON)
                    .body(broadcast.toString())
                    .retrieve()
                    .body(BroadcastResult.class);

            logger.info("Broadcast result Id (" + broadcastResult.getHeader().getId() + ")");
            Instant finish = Instant.now();
            long timeElapsed = Duration.between(start, finish).toMillis();
            logger.info("TimeLOG Time used is : " + timeElapsed);

        } catch (HttpClientErrorException e) {
            logger.error("Throwing message from client (" + e.getMessage() + ") " + crudMessage);
        }
    }

    private static JSONObject getJsonObject(CRUDMessage crudMessage) {
        Map<String, String> dataTypeMap = new HashMap<>();
        dataTypeMap.put("name", "noarkObject");
        dataTypeMap.put("version", "5.5.0");

        Map<String, String> valueMap = new HashMap<>();
        valueMap.put("systemID", crudMessage.systemId());
        valueMap.put("createdDate", crudMessage.createdDate());
        valueMap.put("createdBy", crudMessage.createdBy());
        valueMap.put("eventType", crudMessage.eventType());
        valueMap.put("objectType", crudMessage.objectType());

        Map<String, Map<String, String>> payloadMap = new HashMap<>();
        payloadMap.put("datatype", dataTypeMap);
        payloadMap.put("value", valueMap);
        return new JSONObject(payloadMap);
    }
}
