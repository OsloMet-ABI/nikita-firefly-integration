package app.integration.crud;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public record CRUDMessage(@JsonProperty("systemID") String systemId,
                          @JsonProperty("opprettetAv") String createdBy,
                          @JsonProperty("opprettetDato") String createdDate,
                          @JsonProperty("eventType") String eventType,
                          @JsonProperty("objectType") String objectType
                          )
        implements Serializable {
    @Override
    public String toString() {
        return "CRUDMessage{" +
                "systemId='" + systemId + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", eventType='" + eventType + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
