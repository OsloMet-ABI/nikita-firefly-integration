package app.domain;

import java.util.HashMap;
import java.util.Map;

public class BroadcastResult {
    Header HeaderObject;
    private String localNamespace;
    private String hash;
    private String state;
    Map<String, String> data = new HashMap<>();

    public Header getHeader() {
        return HeaderObject;
    }

    public String getLocalNamespace() {
        return localNamespace;
    }

    public String getHash() {
        return hash;
    }

    public String getState() {
        return state;
    }

    // Setter Methods

    public void setHeader(Header headerObject) {
        this.HeaderObject = headerObject;
    }

    public void setLocalNamespace(String localNamespace) {
        this.localNamespace = localNamespace;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setState(String state) {
        this.state = state;
    }
}
