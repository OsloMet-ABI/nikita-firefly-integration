package app.domain;

import java.util.ArrayList;

public class Header {
    private String id;
    private String type;
    private String txtype;
    private String author;
    private String key;
    private String created;
    private String namespace;
    ArrayList< String > topics = new ArrayList < String > ();
    private String datahash;


    // Getter Methods

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTxtype() {
        return txtype;
    }

    public String getAuthor() {
        return author;
    }

    public String getKey() {
        return key;
    }

    public String getCreated() {
        return created;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getDatahash() {
        return datahash;
    }

    // Setter Methods

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTxtype(String txtype) {
        this.txtype = txtype;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public void setDatahash(String datahash) {
        this.datahash = datahash;
    }
}
