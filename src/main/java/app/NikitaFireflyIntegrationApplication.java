package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NikitaFireflyIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NikitaFireflyIntegrationApplication.class, args);
    }

}
